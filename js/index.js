var map = L.Mapzen.map('map', {
    center: [49.27, 19.965],
    zoom: 12,
    //scene: L.Mapzen.BasemapStyles.BubbleWrap
    scene: L.Mapzen.BasemapStyles.Zinc
});



// var locator = L.Mapzen.locator();
// locator.addTo(map);

var control = L.Routing.control({
    language: 'de',
    waypoints: [
        L.latLng(49.27, 19.90),
        L.latLng(49.28, 20.03)
    ],

    geocoder: L.Control.Geocoder.mapzen('mapzen-qaTu2wV', {
        autocomplete: true,
        placeholder: 'Szukaj',
        params: {
            sources: 'osm',
            'boundary.country': 'PL',
            layers: 'venue,address'
        },
    }),
    reverseWaypoints: true,
    router: L.Routing.mapzen('mapzen-KSkQv79', {
        costing: 'bicycle',
        directions_options: {
        language: 'pl-PL',}
    }),
    formatter: new L.Routing.mapzenFormatter(),
    summaryTemplate: '<div class="start">{name}</div><div class="info {costing}">{distance}, {time}</div>'
}).addTo(map);
